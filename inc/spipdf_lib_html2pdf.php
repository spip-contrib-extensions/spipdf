<?php

/**
 * Génération d'article SPIP au format PDF.
 *
 * @package      spiPDF
 * @author       Collectif SPIP
 * @copyright    2010-2022 Yves Tannier et al
 *
 * @link         https://contrib.spip.net/3719
 * @link         https://git.spip.net/spip-contrib-extensions/spipdf/
 *
 * @license      GPL Gnu Public Licence
 *
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 *
 * @param $html
 * @param $file
 * @param $contexte
 * @return string
 * @throws Exception
 */
function inc_spipdf_lib_html2pdf_dist($html, $file = false, $contexte = []) {

	// nettoyer le HTML et gérer les placements d'image en fonction de la librairie utilisée
	$options_nettoyer = [
		'float' => false,
		'caption' => true,
	];
	$html = spipdf_nettoyer_html($html, $options_nettoyer);


	$dir_librairie_pdf = find_in_path('lib/html2pdf/');
	if (empty($dir_librairie_pdf) or !file_exists($dir_librairie_pdf . 'html2pdf.class.php')) {
		throw new \Exception('Impossible de trouver le répertoire lib/html2pdf/ de la librairie HTML2Pdf');
	}

	// appel de la classe HTML2pdf
	require_once $dir_librairie_pdf . 'html2pdf.class.php';
	try {
		$lang = $GLOBALS['spip_lang'] ?? 'fr';

		if (!empty($contexte['lang'])) {
			$lang = $contexte['lang'];
		}

		// utilisé pour le constructeur de HTML2PDF
		$unicode = (_SPIPDF_CHARSET == 'UTF-8' ? true : false);
		// @deprecated
		if (defined('SPIPDF_UNICODE')) {
			$unicode = SPIPDF_UNICODE;
		}


		// les paramétres d'orientation et de format son écrasé par ceux défini dans la balise <page> du squelette
		$html2pdf = new HTML2PDF('P', _SPIPDF_FORMAT, $lang, $unicode, _SPIPDF_CHARSET);

		// mode debug de HTML2PDF
		if (defined('_SPIPDF_DEBUG_HTML2PDF')) {
			$html2pdf->setModeDebug();
		}

		// police différente selon unicode ou latin
		if ($unicode) {
			$police_caractere = 'FreeSans';
		} else {
			$police_caractere = 'Arial';
		}

		$html2pdf->setDefaultFont($police_caractere);
		$html2pdf->writeHTML($html);

		$html = $html2pdf->Output($file, $file ? 'F' : 'S'); // envoyer le code binaire du PDF dans le flux
	} catch (HTML2PDF_exception $e) {
		throw new \Exception($e->getMessage());
	}

	return spipdf_echappe_special_pdf_chars($html);
}
