<?php

/**
 * Génération d'article SPIP au format PDF.
 *
 * @package      spiPDF
 * @author       Collectif SPIP
 * @copyright    2010-2022 Yves Tannier et al
 *
 * @link         https://contrib.spip.net/3719
 * @link         https://git.spip.net/spip-contrib-extensions/spipdf/
 *
 * @license      GPL Gnu Public Licence
 *
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

// Options pour les marges des PDF, valables seulement pour la librairie mPDF
// définissez vos options par défaut directement dans votre mes_options.php
if (!defined('_SPIPDF_FORMAT')) {
	define('_SPIPDF_FORMAT', 'A4');
}
if (!defined('_SPIPDF_MARGIN_TOP')) {
	define('_SPIPDF_MARGIN_TOP', 20);
}
if (!defined('_SPIPDF_MARGIN_RIGHT')) {
	define('_SPIPDF_MARGIN_RIGHT', 20);
}
if (!defined('_SPIPDF_MARGIN_BOTTOM')) {
	define('_SPIPDF_MARGIN_BOTTOM', 15);
}
if (!defined('_SPIPDF_MARGIN_LEFT')) {
	define('_SPIPDF_MARGIN_LEFT', 15);
}
if (!defined('_SPIPDF_MARGIN_HEADER')) {
	define('_SPIPDF_MARGIN_HEADER', 2);
}
if (!defined('_SPIPDF_MARGIN_FOOTER')) {
	define('_SPIPDF_MARGIN_FOOTER', 2);
}

// Charset (qui peut être défini dans un fichier d'options
if (!defined('_SPIPDF_CHARSET')) {
	// prise en charge du vieux nommage non conventionnel @deprecatef SPIPDF_CHARSET
	define('_SPIPDF_CHARSET', defined('SPIPDF_CHARSET') ? SPIPDF_CHARSET : 'UTF-8');
}

// pour les function unicode2charset
include_spip('inc/charsets');

//
/**
 * traiter la balise <page.. > et ses attributs
 *
 * @param string $html
 * @param array $args
 * @return string
 */
function traite_balise_page($html, &$args) {

	$args = [];
	// on teste la balise page
	if (preg_match('/<page\s(.*)>/iUms', $html, $matches)) {
		// parser les attributs de la balise page
		if (!empty($matches[1])) {
			$balise_page = $matches[1];
			if (preg_match_all('/(.*)=[\'"](.*)[\'"]/iUms', $balise_page, $matcheargs, PREG_SET_ORDER)) {
				foreach ($matcheargs as $m) {
					$k = trim($m[1]);
					$v = trim($m[2]);
					$v = str_replace('mm', '', $v);
					$args[$k] = $v;
				}
			}
		}
		// supprimer <page> et </page>
		$html = str_replace($matches[0], '', $html);
		$html = str_replace('</page>', '', $html);
		return $html;
	}

	return $html;
}

/**
 * traitement principal. avec ce pipeline, le PDF est mis en cache et recalculé "normalement"
 *
 *
 * @param string $html
 *   Le contenu HTML à transformer en PDF
 * @param string/bool $file
 *   Nom du fichier vers lequel enregistrer (uniquement fonctionnel avec mpdf pour l'instant)
 * @return string
 *   Contenu binaire du PDF généré
 */
function spipdf_html2pdf($html, $file = false, $contexte = []) {

	// par defaut
	$librairie_pdf = 'mpdf';
	// choix de la classe de génération via la balise <page lib>
	if (
		preg_match('/\<page\s*.lib_pdf=["\'](.*)["\']/iUms', $html, $match_librairie)
		&& !empty($match_librairie[1])
	) {
		$librairie_pdf = strtolower($match_librairie[1]);
		if (!charger_fonction('spipdf_lib_' . $librairie_pdf, 'inc', true)) {
			die("Librairie PDF $librairie_pdf non implementee dans une fonction inc_spipdf_lib_{$librairie_pdf}_dist()");
		}
	}

	$spipdf_lib = charger_fonction('spipdf_lib_' . $librairie_pdf, 'inc');
	try {
		$html = $spipdf_lib($html, $file, $contexte);
	}
	catch (\Exception $e) {
		spip_log($s = "Echec generation PDF avec $librairie_pdf : " . $e->getMessage(), 'spipdf' . _LOG_ERREUR);
		die($s);
	}

	return $html;
}

/**
 * On échappe les suites de caractères <? pour éviter des erreurs d'évaluation PHP (seront remis en place avec affichage_final)
 * l'erreur d'évaluation est liée à la directive short_open_tag=On dans la configuration de PHP
 *
 * @param string $html
 * @return string
 */
function spipdf_echappe_special_pdf_chars($html) {
	if (strpos($html, '<' . '?') !== false) {
		$html = str_replace('<' . '?', "<\2\2?", $html);
	}

	return $html;
}


/**
 * fonction permettant de merger plusieurs pdfs en un seul
 *
 * @param array $tPDF tableau des chemins des pdfs : ['IMG/pdf/toto.pdf', 'IMG/pdf/titi.pdf' ]
 * @param array $opts tableau d'option :
 *	- outFilePDF : chemin du fichier de sortie : 'tmp/mon_merge.pdf' :
 *		Si un nom de fichier est fourni, on enregistre le fichier,
 *		sinon envoyer le code binaire du PDF dans le flux
 *	- errorInHTML : si oui => affiche dans le fichier de sortie le nom du fichier en erreur (pas de pdf, ou un pdf non valide),
 *	on affiche dans le merge du pdf le nom du fichier
 * @param array &$error un tableau qui sera complété par référence avec le nom des fichiers en erreur
 * @return
 */
function mergePDFFiles(array $tPDF, array $opts, array &$error = []) {
	$outFilePDF = $opts['outFilePDF'] ?? '';
	$errorInHTML = $opts['errorInHTML'] ?? '';

	// les fichiers tmp dans le tmp/ de spip
	if (!defined('_MPDF_TEMP_PATH')) {
		define('_MPDF_TEMP_PATH', sous_repertoire(_DIR_TMP, 'mpdf'));
	}
	require_once _DIR_PLUGIN_SPIPDF . 'vendor/autoload.php';
	if (!class_exists('\Mpdf\Mpdf')) {
		throw new \Exception('Impossible de trouver la classe \Mpdf\Mpdf de la librairie mPdf8');
	}

	$options = [
		'tempDir'    => _MPDF_TEMP_PATH,
		'charset_in' => _SPIPDF_CHARSET,
		'format'     => $args_page['format'] ?? _SPIPDF_FORMAT,
	];

	$mpdf = new \Mpdf\Mpdf($options);
	$nbrPDF = count($tPDF);
	if($nbrPDF) {
		if ($outFilePDF && !file_exists($outFilePDF)) {
			$handle = fopen($outFilePDF, 'w');
			fclose($handle);
		}

		$fileNumber = 1;
		foreach ($tPDF as $PDFName) {
			if (file_exists($PDFName)) {
				$ext  = array_pop(explode('.', $PDFName));
				// Si on est en présence d'une image et non d'un pdf, on insere l'image dans le pdf
				if (in_array($ext, ['png', 'jpg', 'jpeg'])) {
						$mpdf->WriteHTML('<img src="' . $PDFName . '" alt="toto">' );
						if ($fileNumber < $nbrPDF) {
							$mpdf->WriteHTML('<pagebreak />');
						}
				} else {
					try {
						$pagesInFile = $mpdf->SetSourceFile($PDFName);
						for ($i = 1; $i <= $pagesInFile; $i++) {
							$tplId = $mpdf->importPage($i);
							$mpdf->UseTemplate($tplId);
							if (($fileNumber < $nbrPDF) || ($i != $pagesInFile)) {
								$mpdf->WriteHTML('<pagebreak />');
							}
						}
					} catch(Exception $ex) {
						if ($errorInHTML) {
							$mpdf->WriteHTML('Erreur avec le fichier : ' . $PDFName);
							if (($fileNumber < $nbrPDF) || ($i != $pagesInFile)) {
								$mpdf->WriteHTML('<pagebreak />');
							}
						}
						$error[] = $PDFName;
						spip_log($ex, "mpdf_merge_error");
					}
				}
			}
			$fileNumber++;
		}
		$mpdf->Output($outFilePDF);

		/**
		 * Si un nom de fichier est fourni, on enregistre le fichier,
		 * sinon envoyer le code binaire du PDF dans le flux
		 */
		return $mpdf->Output($outFilePDF, $outFilePDF ? 'F' : 'S');
    }
}
