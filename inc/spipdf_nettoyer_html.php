<?php

/**
 * Génération d'article SPIP au format PDF.
 *
 * @package      spiPDF
 * @author       Collectif SPIP
 * @copyright    2010-2022 Yves Tannier et al
 *
 * @link         https://contrib.spip.net/3719
 * @link         https://git.spip.net/spip-contrib-extensions/spipdf/
 *
 * @license      GPL Gnu Public Licence
 *
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * repris dans le plugin article_pdf
 * TODO a modifier ? mettre a jour ?
 * https://git.spip.net/spip-contrib-extensions/article_pdf/src/branch/master/article_pdf_fonctions.php
 *
 * @param string $texte
 * @return string
 */
function spipdf_first_clean($texte) {

	// supprimer les remarques HTML (du Couteau Suisse ?)
	$texte = preg_replace(',<!-- .* -->,msU', '', $texte);

	$trans = [];
	$trans["<br />\n"] = '<BR>'; // Pour éviter que le \n ne se tranforme en espace dans les <DIV class=spip_code> (TT, tag SPIP : code)

	// gestion d'un encodage latin1
	if (_SPIPDF_CHARSET == 'ISO-8859-15' || _SPIPDF_CHARSET == 'iso-8859-15') {
		$trans['&#176;'] = '°';
		$trans['&#339;'] = 'oe';
		$trans['&#8211;'] = '-';
		$trans['&#8216;'] = '\'';
		$trans['&#8217;'] = '\'';
		$trans['&#8220;'] = '"';
		$trans['&#8221;'] = '"';
		$trans['&#8230;'] = '...';
		$trans['&#8364;'] = 'Euros';
		$trans['&ucirc;'] = 'û';
		$trans['->'] = '-»';
		$trans['<-'] = '«-';
		$trans['&mdash;'] = '-';
		$trans['&deg;'] = '°';
		$trans['œ'] = 'oe';
		$trans['Œ'] = 'OE';
		$trans['…'] = '...';
		$trans['&euro;'] = 'Euros';
		$trans['€'] = 'Euros';
		$trans['&copy;'] = '©';
	}

	// pas d'insécable
	//$trans['&nbsp;'] = ' ';

	// certains titles font paniquer l'analyse
	// TODO : a peaufiner car ils sont necessaires pour les signets
	// <bookmark title="Compatibilité" level="0" ></bookmark>
	// http://wiki.spipu.net/doku.php?id=html2pdf:fr:v4:bookmark
	//$texte = preg_replace(',title=".*",msU', 'title=""', $texte);

	$texte = strtr($texte, $trans);
	if (_SPIPDF_CHARSET == 'UTF-8') {
		$texte = charset2unicode($texte);
	} else {
		$texte = unicode2charset(charset2unicode($texte), _SPIPDF_CHARSET); // Repasser tout dans le charset demandé
	}

	// Décoder les codes HTML dans le charset final
	$texte = html_entity_decode($texte, ENT_NOQUOTES, _SPIPDF_CHARSET);

	return $texte;
}

/**
 * Callback preg
 * @param array $matches
 * @return string
 */
function spipdf_remplaceDocumentLeftRight_wfloat($matches) {
	return spipdf_remplaceDocumentLeftRight($matches, true);
}

/**
 * Callback preg
 * @param array $matches
 * @return string
 */
function spipdf_remplaceDocumentLeftRight($matches, $float = false) {
	$align = $matches[1];
	$html = $matches[2];
	$figure = extraire_balise($html, 'figure');
	$figure = explode('>', $figure);
	$figure = reset($figure) . '>';
	if ($float) {
		$figure_mod = ajouter_class($figure, 'pdf_img_float_' . $align);
	}
	else {
		$figure_mod = inserer_attribut($figure, 'align', $align);
	}
	$figure_mod = inserer_attribut($figure_mod, 'style', 'padding:5px;float:' . $align);
	return str_replace($figure, $figure_mod, $matches[2]);
}

/**
 * Callback preg
 * @param array $matches
 * @return string
 */
function spipdf_remplaceDocumentCenter($matches) {
	$matches[1] = 'center';
	return spipdf_remplaceDocumentLeftRight($matches, false);
}


/**
 * Callback preg
 * @param array $matches
 * @return string
 */
function spipdf_remplaceSpanCenter($matches) {
	return $matches[1];
}

/**
 * Callback preg
 * @param array $matches
 * @return string
 */
function spipdf_remplaceIdParName($matches) {
	return str_replace('id=\'', 'name=\'', $matches[0]);
}


/**
 * Callback preg
 * @param array $matches
 * @return string
 */
function spipdf_remplaceCaption($matches) {
	$table = '<table style="border:none;"' . $matches[1] . '<tr><td style="text-align: center;border:none;">' . $matches[2] . '</td></tr>';
	$table .= '<tr><td style="border:none;">';
	$table .= '<table' . $matches[1] . $matches[3] . '</table>';
	$table .= '</td></tr></table>';

	return $table;
}

/**
 * Callback preg
 * @param array $matches
 * @return string
 */
function spipdf_remplaceTextarea($matches) {
	return '<pre' . $matches[1] . '>' . entites_html($matches[2]) . '</pre>';
}

/**
 * Callback preg
 * @param array $matches
 * @return string
 */
function spipdf_remplaceCode($matches) {
	return '<tt class="spip_code">' . entites_html($matches[2]) . '</tt>';
}

/**
 * Nettoyer le markup et adapter le balisage des documents
 *
 * @param string $html
 * @param array $params_pdf
 * @return string
 */
function inc_spipdf_nettoyer_html_dist($html, $params_pdf = []) {

	// reformater les documents
	$pattern_left_right = ',<div class=[\'"]spip_document_[0-9]+[^>]*spip_document_(left|right)[^>]*>\s*(<figure[^>]*>(.*)</figure>)\s*<\/div>,Uims';
	$html = preg_replace_callback($pattern_left_right, 'spipdf_remplaceDocumentLeftRight' . (empty($params_pdf['float']) ? '' : '_wFloat'), $html);

	$pattern_others = ',<div class=[\'"]spip_document_[0-9]+([^>]*)>\s*(<figure[^>]*>(.*)</figure>)\s*<\/div>,Uims';
	#$html = preg_replace_callback($pattern_others, 'spipdf_remplaceDocumentCenter', $html);


	// replacer id par name pour les notes
	$patterns_note = '/<a[^>]*href[^>]*class=\'spip_note\'[^>]*>/iUms';
	$html = preg_replace_callback($patterns_note, 'spipdf_remplaceIdParName', $html);

	// remplacer les captions
	if (!empty($params_pdf['caption'])) {
		$patterns_caption = '/<table(.*)<caption>(.*)<\/caption>(.*)<\/table>/iUms';
		$html = preg_replace_callback($patterns_caption, 'spipdf_remplaceCaption', $html);
	}

	// tableaux centré
	$html = preg_replace('/<table/iUms', '<table align="center"', $html);

	// balise cadre
	$html = preg_replace_callback(',<textarea\b([^>]*)>(.*)</textarea>,Uims', 'spipdf_remplaceTextarea', $html);

	// balise code
	$html = preg_replace_callback(',<code([^>]*)>(.*)</code>,Uims', 'spipdf_remplaceCode', $html);

	// gestion des caractères spéciaux et de charset
	$html = spipdf_first_clean($html);

	return $html;
}
