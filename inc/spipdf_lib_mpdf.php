<?php

/**
 * Génération d'article SPIP au format PDF.
 *
 * @package      spiPDF
 * @author       Collectif SPIP
 * @copyright    2010-2022 Yves Tannier et al
 *
 * @link         https://contrib.spip.net/3719
 * @link         https://git.spip.net/spip-contrib-extensions/spipdf/
 *
 * @license      GPL Gnu Public Licence
 *
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * @param $html
 * @param $file
 * @param $contexte
 * @return string
 * @throws Exception
 */
function inc_spipdf_lib_mpdf_dist($html, $file = false, $contexte = []) {

	// l'ancienne mpdf n'est pas compatible PHP 8+, on delegue donc a mPdf8
	if (PHP_VERSION_ID >= 80000) {
		$spipdf_lib = charger_fonction('spipdf_lib_mpdf8', 'inc');
		return $spipdf_lib($html, $file, $contexte);
	}

	// si il y a des options dans la balise page
	// http://mpdf1.com/manual/index.php?tid=307

	// nettoyer le HTML et gérer les placements d'image en fonction de la librairie utilisée
	$options_nettoyer = [
		'float' => true,
		'caption' => true,
	];
	$html = spipdf_nettoyer_html($html, $options_nettoyer);

	// traiter la balise page pour extraire les arguments de format
	$args_page = [];
	$html = traite_balise_page($html, $args_page);

	if (defined('_MPDF_PATH')) {
		$dir_librairie_pdf = _MPDF_PATH;
		if (empty($dir_librairie_pdf) or !is_dir(_MPDF_PATH)) {
			throw new \Exception('Impossible de trouver le répertoire _MPDF_PATH=' . _MPDF_PATH . ' de la librairie mPdf');
		}
	}
	else {
		$dir_librairie_pdf = find_in_path('lib/mpdf/');
		define('_MPDF_PATH', $dir_librairie_pdf);
		if (empty($dir_librairie_pdf)) {
			throw new \Exception('Impossible de trouver le répertoire lib/mpdf/ de la librairie mPdf');
		}
	}

	// les fichiers tmp dans le tmp/ de spip
	if (!defined('_MPDF_TEMP_PATH')) {
		define('_MPDF_TEMP_PATH', sous_repertoire(_DIR_TMP, 'mpdf'));
	}
	if (!defined('_MPDF_TTFONTDATAPATH')) {
		define('_MPDF_TTFONTDATAPATH', sous_repertoire(_DIR_CACHE, 'ttfontdata'));
	}

	include_once _MPDF_PATH . 'mpdf.php';

	// la classe mPDF
	$mpdf = new mPDF(
		_SPIPDF_CHARSET,
		$args_page['format'] ?? _SPIPDF_FORMAT,
		0,
		'',
		$args_page['backleft'] ?? _SPIPDF_MARGIN_LEFT,
		$args_page['backright'] ?? _SPIPDF_MARGIN_RIGHT,
		$args_page['backtop'] ?? _SPIPDF_MARGIN_TOP,
		$args_page['backbottom'] ?? _SPIPDF_MARGIN_BOTTOM,
		$args_page['margin_header'] ?? _SPIPDF_MARGIN_HEADER,
		$args_page['margin_footer'] ?? _SPIPDF_MARGIN_FOOTER
	);


	$mpdf->WriteHTML($html);
	/**
	 * Si un nom de fichier est fourni, on enregistre le fichier,
	 * sinon envoyer le code binaire du PDF dans le flux
	 */
	$html = $mpdf->Output($file, $file ? 'F' : 'S');

	return spipdf_echappe_special_pdf_chars($html);
}
