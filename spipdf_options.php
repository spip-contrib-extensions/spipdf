<?php

/**
 * Génération d'article SPIP au format PDF.
 *
 * @package      spiPDF
 * @author       Collectif SPIP
 * @copyright    2010-2022 Yves Tannier et al
 *
 * @link         https://contrib.spip.net/3719
 * @link         https://git.spip.net/spip-contrib-extensions/spipdf/
 *
 * @license      GPL Gnu Public Licence
 *
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Wrapper pour la fonction spipdf_nettoyer_html
 * @see inc_spipdf_nettoyer_html_dist()
 *
 * @param string $html
 * @param array $params_pdf
 * @return string
 */
function spipdf_nettoyer_html($html, $params_pdf = []) {
	$spipdf_nettoyer_html = charger_fonction('spipdf_nettoyer_html', 'inc');

	$html = $spipdf_nettoyer_html($html, $params_pdf);

	// Debug = voir le html sans génération de PDF
	if (
		isset($_GET['debug_spipdf'])
		and include_spip('inc/autoriser')
		and autoriser('debug')
	) {
		echo $html;
		exit;
	}

	return $html;
}


/**
 * Wrapper pour spipdf_html2pdf
 * @param string $html
 * @param bool $file
 * @return string
 */
function filtre_spipdf_html2pdf_dist($Pile, $html, $file = false) {
	// recuperer le env du squelette
	$env = $Pile[0] ?? [];

	include_spip('inc/spipdf');
	return spipdf_html2pdf($html, $file, $env);
}

/**
 * On rétablit les <? du code PDF si necessaire
 * on n'agit que sur les pages non html.
 *
 * @param string $texte
 *
 * @return string
 */
function spipdf_affichage_final($texte) {
	if (
		$GLOBALS['html'] == false
		and strpos($texte, "<\2\2?") !== false
	) {
		$texte = str_replace("<\2\2?", '<' . '?', $texte);
	}

	return $texte;
}

/**
 * Ne pas permettre d'aller chercher un fond en sous-repertoire dans spipdf.html.
 *
 * @param $fond
 *
 * @return mixed
 */
function spipdf_securise_fond($fond) {
	$fond = str_replace('/', '_', $fond);
	$fond = str_replace('\\', '_', $fond);

	return $fond;
}
