<?php

/**
 * Génération d'article SPIP au format PDF.
 *
 * @package      spiPDF
 * @author       Collectif SPIP
 * @copyright    2010-2022 Yves Tannier et al
 *
 * @link         https://contrib.spip.net/3719
 * @link         https://git.spip.net/spip-contrib-extensions/spipdf/
 *
 * @license      GPL Gnu Public Licence
 *
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * @param $html
 * @param $file
 * @param $contexte
 * @return string
 * @throws Exception
 */
function inc_spipdf_lib_mpdf8_dist($html, $file = false, $contexte = []) {
	// lib mpdf 8 charger via autoloader
	// https://mpdf.github.io/installation-setup/installation-v7-x.html

	// nettoyer le HTML et gérer les placements d'image en fonction de la librairie utilisée
	$options_nettoyer = [
		'float' => true,
		'caption' => true,
	];
	$html = spipdf_nettoyer_html($html, $options_nettoyer);

	// traiter la balise page pour extraire les arguments de format
	$args_page = [];
	$html = traite_balise_page($html, $args_page);


	// les fichiers tmp dans le tmp/ de spip
	if (!defined('_MPDF_TEMP_PATH')) {
		define('_MPDF_TEMP_PATH', sous_repertoire(_DIR_TMP, 'mpdf'));
	}

	require_once _DIR_PLUGIN_SPIPDF . 'vendor/autoload.php';
	if (!class_exists('\Mpdf\Mpdf')) {
		throw new \Exception('Impossible de trouver la classe \Mpdf\Mpdf de la librairie mPdf8');
	}

	$options = [
		'tempDir'    => _MPDF_TEMP_PATH,
		'charset_in' => _SPIPDF_CHARSET,
		'format'     => $args_page['format'] ?? _SPIPDF_FORMAT,
		//'mode' => '1',
	];

	/**
	 * Pipeline pour l'ajout de fonts persos
	 * Exemple d'usage :
	 * 
	 * 	[
	 * 		'dir' => _DIR_PLUGIN_POLE . 'polices',
	 * 		'data' => [
	 * 			"sourcesans" => [
	 * 				'R' => "SourceSansPro-Regular.ttf",
	 * 				'B' => "SourceSansPro-Bold.ttf",
	 * 				'I' => "SourceSansPro-It.ttf",
	 * 				'BI' => "SourceSansPro-BoldIt.ttf",
	 * 			],
	 * 			"sourcesanslight" => [
	 * 				'R' => "SourceSansPro-Light.ttf",
	 * 				'I' => "SourceSansPro-LightIt.ttf",
	 * 			],
	 * 			"sourcesansblack" => [
	 * 				'R' => "SourceSansPro-Black.ttf",
	 * 			],
	 * 		]
	 * 	]
	 * 
	 */
	$fonts = pipeline('spipdf_mpdf_fonts', []);
	if (count($fonts) > 0) {
		// récupérer les valeurs de font par défaut
		$defaultConfig = (new \Mpdf\Config\ConfigVariables())->getDefaults();
		$fontDirs = $defaultConfig['fontDir'];
		$defaultFontConfig = (new \Mpdf\Config\FontVariables())->getDefaults();
		$fontData = $defaultFontConfig['fontdata'];
		// et les compléter par celles issues du pipeline
		foreach($fonts as $font) {
			$options['fontDir'] = array_merge($fontDirs, [$font['dir']]);
			$options['fontdata'] = $fontData + $font['data'];
		}
	}

	$mpdf = new \Mpdf\Mpdf($options);
	$mpdf->WriteHTML($html);

	/**
	 * Si un nom de fichier est fourni, on enregistre le fichier,
	 * sinon envoyer le code binaire du PDF dans le flux
	 */
	$html = $mpdf->Output($file, $file ? 'F' : 'S');

	return spipdf_echappe_special_pdf_chars($html);
}
