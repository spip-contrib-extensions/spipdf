<?php

/**
 * Génération d'article SPIP au format PDF.
 *
 * @package      spiPDF
 * @author       Collectif SPIP
 * @copyright    2010-2022 Yves Tannier et al
 *
 * @link         https://contrib.spip.net/3719
 * @link         https://git.spip.net/spip-contrib-extensions/spipdf/
 *
 * @license      GPL Gnu Public Licence
 *
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * domPDF beta 0.6 EXPERIMENTAL
 *
 * @param $html
 * @param $file
 * @param $contexte
 * @return string
 * @throws Exception
 */
function inc_spipdf_lib_dompdf_dist($html, $file = false, $contexte = []) {

	// nettoyer le HTML et gérer les placements d'image en fonction de la librairie utilisée
	$options_nettoyer = [
		'float' => true,
		'caption' => true,
	];
	$html = spipdf_nettoyer_html($html, $options_nettoyer);

	// traiter la balise page pour extraire les arguments de format
	$args_page = [];
	$html = traite_balise_page($html, $args_page);


	$dir_librairie_pdf = find_in_path('lib/dompdf/');
	if (empty($dir_librairie_pdf) or !file_exists($dir_librairie_pdf . 'dompdf_config.inc.php')) {
		throw new \Exception('Impossible de trouver le répertoire lib/dompdf/ de la librairie DOMPdf');
	}

	// le chemin relatif vers DOMPdf
	require_once $dir_librairie_pdf . 'dompdf_config.inc.php';

	$dompdf = new DOMPDF();
	$dompdf->load_html($html, _SPIPDF_CHARSET);
	$dompdf->set_paper($args_page['format'] ?? _SPIPDF_FORMAT);
	$dompdf->render();
	$html = $dompdf->output();

	if ($file) {
		include_spip('inc/flock');
		ecrire_fichier($file, $html);
	}

	return spipdf_echappe_special_pdf_chars($html);
}
