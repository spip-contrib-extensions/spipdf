# Version avec la lib mpdf >= 8 sur la branche mpdf

> ex de mise en page en css
https://mpdf.github.io/paging/using-page.html

```html
<page lib_pdf="mpdf8" format="A4">
	<html>
		<head>
			<style>
				@page {
					size: auto;
					header: html_myHeader1;
					footer: html_myFooter1;
					margin-header: 3mm;
					margin-footer: 3mm;
					margin-top: 8mm;
					margin-left: 10mm;
					margin-right: 10mm;
				}
			</style>
		</head>
		<body>
			<htmlpageheader name="myHeader1" >
				<div class="header">
					Page {PAGENO}/{nbpg}
				</div>
			</htmlpageheader>

			<htmlpagefooter name="myFooter1" style="display:none">
				<div>
					mon footer
				</div>
			</htmlpagefooter>

			<div class="mon_contenu">
				Lorem ipsum dolor sit amet, ... ,  no sea takimata sanctus est Lorem ipsum dolor sit amet.
			</div>
		</body>
	</html>
</page>
```
